import React from "react";

type ButtonProps = {
  className: string;
  type: "button" | "submit" | "reset" | undefined;
  btnName: string;
};

export const Button = React.memo(
  ({
    className = "btn btn-primary",
    type = "button",
    btnName,
    ...props
  }: ButtonProps) => {
    return (
      <button
        className={className}
        type={type}
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
        {...props}
      >
        {btnName ?? <span className="navbar-toggler-icon"></span>}
      </button>
    );
  }
);
