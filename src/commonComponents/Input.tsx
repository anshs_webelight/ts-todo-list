import React from "react";

type InputProps = {
  type: string;
  value: string;
  className: string;
  placeholder: string;
  onChange: (e: any) => void;
};
export const Input = React.memo(
  ({ type, value, className, placeholder, onChange }: InputProps) => {
    return (
      <input
        type={type}
        value={value}
        className={className}
        placeholder={placeholder}
        onChange={onChange}
      />
    );
  }
);
