import React from "react";

type ImageProps = {
  src: string;
  alt: string;
  height: string;
  width: string;
};

export const Image = React.memo(
  ({ src, alt, height = "100px", width = "100px" }: ImageProps) => {
    return <img src={src} alt={alt} height={height} width={width} />;
  }
);
