import React from "react";
import Button from "@material-ui/core/Button";

type Color = "default" | "primary" | "secondary" | "inherit" | undefined;
type Variant = "text" | "contained" | "outlined" | undefined;
type Type = "button" | "submit" | "reset" | undefined;

type ButtonComponentProps = {
  type: Type;
  color: Color;
  btnName: string;
  variant?: Variant;
  className?: string;
  onClick?: () => void;
};
export const ButtonComponent = ({
  variant = "contained",
  type,
  color = "default",
  className,
  btnName,
  onClick,
}: ButtonComponentProps) => {
  return (
    <Button
      variant={variant}
      color={color}
      type={type}
      className={className}
      onClick={onClick}
    >
      {btnName ?? <span className="navbar-toggler-icon"></span>}
    </Button>
  );
};
