import "./Layout.css";

export const Footer = () => {
  return (
    <footer className=" footer bg-dark text-light">
      <p className="text-center">
        Copyrights&nbsp; &copy; &nbsp; 2022 &nbsp; myTodoList.com
      </p>
    </footer>
  );
};
