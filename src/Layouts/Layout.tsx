import React from "react";
import { Footer } from "./Footer";
import { Header } from "./Header";

export const Layout = React.memo(({ component }: any) => {
  return (
    <>
      <Header title="My Todos-list" />
      <br />
      {component}
      <Footer />
    </>
  );
});
