import React, { useContext } from "react";
import { TodosContext } from "../App";
import { AddTodo, EditTodo, TodosList } from "../Todos";

type TodosProps = {
  isEdit: boolean;
};
export const Todos = React.memo(({ isEdit }: TodosProps) => {
  const todoContext = useContext(TodosContext);
  return (
    <>
      <br />
      <br />
      <div className="row">
        <div className="col-md-6 col-sm-8">
          {isEdit ? <EditTodo /> : <AddTodo />}
        </div>
        <div className="col-md-6 col-sm-4">
          <TodosList todos={todoContext?.todos} />
        </div>
      </div>
    </>
  );
});
