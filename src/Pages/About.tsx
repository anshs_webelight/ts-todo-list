import { Layout } from "../Layouts/Layout";

const Component = () => {
  return <div>About works!</div>;
};

export const About = () => {
  return <Layout component={<Component />} />;
};
