import { Link } from "react-router-dom";
import { useFetch } from "../customHooks/userFetch";
import { appConfig } from "../../config";
import { Image, Loader } from "../../commonComponents";
import "./Users.css";

export const Users = () => {
  const { isLoading, users } = useFetch(appConfig.githubUsersApi as string);

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <div className="container">
          <h3 className="userHeader">Github Users</h3>
          <hr />
          <ul className="userList">
            {users.map((user) => {
              const { id, login, avatar_url } = user;

              return (
                <li key={id}>
                  <div className="row">
                    <div className="col">
                      <Image
                        src={avatar_url}
                        alt={login}
                        height="80px"
                        width="80px"
                      />
                    </div>
                    <div className="col">
                      <div>
                        <h4 className="user-name">{login}</h4>
                        <Link to={`/user/${id}`} className="view-more-link">
                          View more..
                        </Link>
                      </div>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      )}
    </>
  );
};
