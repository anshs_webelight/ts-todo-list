import React, { useContext } from "react";
import { FiEdit } from "react-icons/fi";
import { RiDeleteBinLine } from "react-icons/ri";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { TodosContext } from "../App";
import "./Todo.css";

type TodoItemProps = {
  id: number;
  title: string;
  description: string;
};
export const TodoItem = React.memo(
  ({ id, title, description }: TodoItemProps) => {
    /**
     * getting onDelete, onEdit method from parent component without passing
     *     throgh all the child components in between */
    const todoContext = useContext(TodosContext);
    return (
      <div className="row todo-item">
        <div className="col-md-3">
          <span>{title}</span>
        </div>
        <div className="col-md-3">
          <span>{description}</span>
        </div>
        <div className="col-md-3">
          <span>
            <FiEdit
              className="btn-edit"
              onClick={() => {
                todoContext?.onEdit(id);
              }}
            />
            <RiDeleteBinLine
              className="btn-delete"
              onClick={() => {
                const confirmBox = window.confirm(
                  "Do you want to delete this todo?"
                );
                if (confirmBox === true) {
                  toast.info("Todo deleted successfully!", {
                    position: "top-right",
                    autoClose: 1500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                  });
                  todoContext?.onDelete(id);
                }
              }}
            />
          </span>
        </div>
        <div className="col-md-3"></div>
        <br />
        <ToastContainer />
      </div>
    );
  }
);
