import React, { useContext, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { TodosContext } from "../../App";
import { Input, ButtonComponent } from "../../commonComponents";
import "../Todo.css";

export const AddTodo = () => {
  const todoContext = useContext(TodosContext);
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");

  const submit = (e: any) => {
    e.preventDefault();
    if (!title || !desc) {
      alert("Title or Description cannot be empty!");
    } else {
      todoContext?.addTodo(title, desc);
      toast.success("Todo added successfully!", {
        position: "top-right",
        autoClose: 1500,
      });
      setTitle("");
      setDesc("");
    }
  };
  return (
    <>
      <form className="add-todo-form" onSubmit={submit}>
        <h3 className="header add-todo-header">Add Todo</h3>
        <hr />
        <div className="mb-3">
          <label htmlFor="title" className="form-label labelStyle">
            Title :-
          </label>
          <Input
            type="text"
            className="form-control"
            value={title}
            placeholder="Enter your Title"
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="description" className="form-label labelStyle">
            Description :-
          </label>
          <Input
            type="text"
            className="form-control"
            value={desc}
            placeholder="Enter your Description"
            onChange={(e) => setDesc(e.target.value)}
          />
        </div>
        <br />
        <ButtonComponent
          className="btn btn-success"
          type="submit"
          color="primary"
          btnName="Add"
        />
        <ToastContainer />
      </form>
    </>
  );
};
