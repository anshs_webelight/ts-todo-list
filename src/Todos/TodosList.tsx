import React from "react";
import { TodoItem } from "./TodoItem";

export const TodosList = React.memo(({ todos }: any) => {
  const headerStyle = {
    color: "red",
    marginTop: "15px",
    marginBottom: "20px",
  };

  return (
    <div className="container ms-2 mt-2">
      <h3 style={headerStyle}>
        {todos.length === 0 ? "No Todos to display!" : "Todo List"}
      </h3>
      <br />
      {todos.length !== 0
        ? todos.map((todo: any) => {
            return <TodoItem key={todo.id} {...todo} />;
          })
        : ""}
    </div>
  );
});
