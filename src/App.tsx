import { useState, useEffect, createContext, useCallback } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Todos, About, Users, Person } from "./Pages";
import "./App.css";
import { NotFound } from "./commonComponents/404Page/NotFound";
import { Layout } from "./Layouts/Layout";

// create a context
const TodosContext = createContext<TodoContextInterface | null>(null);

type Todo = {
  id: number;
  title: string;
  description: string;
};
type ArrowFunction = (id: number) => void;
interface TodoContextInterface {
  todos: any;
  isEdit: boolean;
  todoToBeEdited: any;
  editTodo: (updatedTodo: Todo) => void;
  onCancelBtn: () => void;
  addTodo: (title: string, description: string) => void;
  onDelete: ArrowFunction;
  onEdit: ArrowFunction;
}

function App() {
  const initTodo =
    localStorage.getItem("todos") === null
      ? []
      : JSON.parse(localStorage.getItem("todos") as string);

  // todos State
  const [todos, setTodos] = useState(initTodo);
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  // Add new Todo
  const addTodo = useCallback(
    (title: string, desc: string) => {
      const newTodo = {
        id: Math.floor(Math.random() * 100000000),
        title,
        description: desc,
      };
      if (todos.length === 0) {
        setTodos([newTodo]);
      }
      console.log("new Todo :", newTodo);
      setTodos([...todos, newTodo]);

      localStorage.setItem("todos", JSON.stringify(todos));
    },
    [todos]
  );

  // Delete Todo
  const onDelete = (id: number) => {
    console.log("Deleting todo with Id: ", id);

    setTodos(
      todos.filter((todo: Todo) => {
        return todo.id !== id;
      })
    );
  };

  const [isEdit, setEditMode] = useState(false);
  const [todoToBeEdited, setTodoToBeEdited] = useState();

  const onEdit = useCallback(
    (id: number) => {
      setEditMode(true);
      const myTodo = todos.find((todo: Todo) => todo.id === id);
      console.log("Going to edit : ", myTodo);

      setTodoToBeEdited(myTodo);
    },
    [todos]
  );

  // Edit Todo
  const editTodo = (updatedTodo: Todo) => {
    const updatedTodos = todos.filter((todo: Todo) => {
      if (todo.id === updatedTodo.id) {
        todo.id = updatedTodo.id;
        todo.title = updatedTodo.title;
        todo.description = updatedTodo.description;

        return todo;
      }

      return todo;
    });

    setTodos(updatedTodos);
  };

  const onCancelBtn = useCallback(() => {
    setEditMode(false);
  }, []);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <TodosContext.Provider
            value={{
              todos,
              isEdit,
              todoToBeEdited,
              editTodo,
              onCancelBtn,
              addTodo,
              onDelete,
              onEdit,
            }}
          >
            <Layout component={<Todos isEdit={isEdit} />} />
          </TodosContext.Provider>
        </Route>
        <Route exact path="/about" component={About}></Route>
        <Route exact path="/users">
          <Layout component={<Users />} />
        </Route>
        <Route
          path="/user/:id"
          children={<Layout component={<Person />} />}
        ></Route>
        <Route path="*" component={NotFound}></Route>
      </Switch>
    </Router>
  );
}

export default App;
export { TodosContext };
